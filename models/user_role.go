package models

type Role string

const (
	RoleAdminRoot        = "administrator"
	RoleStaff            = "staff"
	RoleManager          = "manager"
	RoleDeviceManagement = "device_management"
	RoleGiftStaff        = "gift_staff"
)
