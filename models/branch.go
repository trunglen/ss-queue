package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type Branch struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string  `bson:"name" json:"name"`
	Address              string  `bson:"address" json:"address"`
	Lat                  float32 `bson:"lat" json:"lat"`
	Lng                  float32 `bson:"lng" json:"lng"`
	CustomerID           string  `bson:"customer_id" json:"customer_id"`
}

func (b *Branch) Create() error {
	return branchTable.Create(b)
}

func (b *Branch) Update() error {
	return branchTable.UpdateID(b.ID, bson.M{
		"name":    b.Name,
		"address": b.Address,
		"lat":     b.Lat,
		"lng":     b.Lng,
	})
}

func GetAllBranches() ([]*Branch, error) {
	var branchs = []*Branch{}
	return branchs, branchTable.FindWhereNewest(bson.M{}, &branchs)
}

func DeleteBranchByID(id string) error {
	return branchTable.DeleteByID(id)
}
