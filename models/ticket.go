package models

import (
	"time"

	"github.com/jinzhu/now"
	"github.com/spf13/viper"
	"github.com/trunglen/g/x/math"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb/util/timer"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Ticket struct {
	mongodb.DefaultModel `bson:",inline"`
	TransactionID        string      `bson:"transaction_id" json:"transaction_id" structs:"transaction_id"`
	CTime                int64       `bson:"ctime" json:"ctime,omitempty" structs:"ctime"`
	MTime                int64       `bson:"mtime" json:"mtime,omitempty" structs:"mtime"`
	BranchID             string      `bson:"branch_id" json:"branch_id" structs:"branch_id"`
	BranchName           string      `bson:"branch_name" json:"branch_name" structs:"branch_name"`
	KioskID              string      `bson:"kiosk_id" json:"kiosk_id" structs:"kiosk_id"`
	ServiceID            string      `bson:"service_id" json:"service_id,omitempty" structs:"service_id"`
	ServiceName          string      `bson:"service_name" json:"service_name,omitempty" structs:"service_name"`
	Services             []string    `bson:"services" json:"services,omitempty" structs:"services"`
	CounterID            string      `bson:"counter_id" json:"counter_id" structs:"counter_id"`
	CounterName          string      `bson:"counter_name" json:"counter_name" structs:"counter_name"`
	Counters             []string    `bson:"counters" json:"counters,omitempty" structs:"counters"`
	UserID               string      `bson:"user_id" json:"user_id,omitempty" structs:"user_id"`
	UserName             string      `bson:"user_name" json:"user_name,omitempty" structs:"user_name"`
	CNum                 string      `bson:"cnum" json:"cnum" structs:"cnum"`
	CallVoice            string      `bson:"-" json:"call_voice,omitempty" structs:"call_voice"`
	TNum                 string      `bson:"tnum" json:"tnum" structs:"tnum"`
	PrintedTicketCount   int64       `bson:"-" json:"printed_ticket_count" structs:"printed_ticket_count"`
	WaitingTicketCount   int64       `bson:"-" json:"waiting_ticket_count" structs:"waiting_ticket_count"`
	Priority             int         `bson:"priority" json:"priority" structs:"priority"`
	Tracks               TrackList   `bson:"tracks" json:"tracks" structs:"tracks"`
	Online               bool        `bson:"online" json:"online" structs:"online"`
	NWaiting             int         `bson:"nwaiting" json:"nwaiting" structs:"nwaiting"`
	Rating               int         `bson:"rating" json:"rating" structs:"rating"`
	Stime                int         `bson:"stime" json:"stime" structs:"stime"`
	Wtime                int64       `bson:"wtime" json:"wtime" structs:"wtime"`
	Lang                 string      `bson:"lang" json:"lang" structs:"lang"`
	State                TicketState `bson:"state" json:"state" structs:"state"`
	ServingCounters      string      `bson:"-" json:"serving_counters" structs:"serving_counters"`
}

type TicketState string

const (
	TicketStateUnknown   = TicketState("unknown")
	TicketStateWaiting   = TicketState("waiting")
	TicketStateServing   = TicketState("serving")
	TicketStateMissed    = TicketState("missed")
	TicketStateFinished  = TicketState("finished")
	TicketStateCancelled = TicketState("cancelled")
)

func (t *Ticket) UpdateServingState() error {
	return ticketTable.UpdateID(t.ID, bson.M{
		"state": t.State,
		"cnum":  t.CNum,
	})
}

func (t *Ticket) Call(userID string, counterID string, serviceID string) error {
	t.Wtime = time.Now().Unix() - t.CTime
	return t.updateState(TicketStateServing, userID, counterID, serviceID, nil, nil)
}

func (t *Ticket) Finish() error {
	var services = []string{}
	for _, s := range t.Services {
		if s != t.ServiceID {
			services = append(services, s)
		}
	}
	if err := t.updateState(TicketStateFinished, t.UserID, t.CounterID, t.ServiceID, services, nil); err != nil {
		return err
	}
	return nil
}

var TransactionIdMaker = math.RandStringMaker{
	Length: 24,
	Prefix: "tr",
}

func (t *Ticket) ReQueue(services []string, counters []string) error {
	t.TransactionID = TransactionIdMaker.Next()
	// t.TicketPriority.MovedTicket++
	return t.updateState(TicketStateWaiting, "", "", "", services, counters)
}

func (t *Ticket) Restore() error {
	t.TransactionID = TransactionIdMaker.Next()
	// if t.Tracks.WasServed() {
	// 	t.TicketPriority.RestoreTicket++
	// }
	return t.updateState(TicketStateWaiting, "", t.CounterID, t.ServiceID, t.Services, nil)
}

func (t *Ticket) Rate() error {
	// if t.Tracks.WasServed() {
	// 	t.TicketPriority.RestoreTicket++
	// }
	return ticketTable.UpdateID(t.ID, bson.M{
		"rating": t.Rating,
	})
}

func (t *Ticket) Move(counter string) error {
	var tracks = t.Tracks
	var last = &tracks[len(tracks)-1]
	last.CounterID = counter
	t.CounterID = counter
	t.State = TicketStateWaiting
	return ticketTable.UpdateID(t.ID, bson.M{
		"counter_id": counter,
		"state":      TicketStateWaiting,
		"tracks":     tracks,
	})

}

func (t *Ticket) MoveService(serviceID, serviceName string, priority int) error {
	var tracks = t.Tracks
	var last = &tracks[len(tracks)-1]
	last.ServiceID = serviceID
	t.State = TicketStateWaiting

	err := ticketTable.UpdateID(t.ID, bson.M{
		"service_id":   serviceID,
		"counter_id":   "",
		"counter_name": "",
		"service_name": serviceName,
		"priority":     priority,
		"state":        TicketStateWaiting,
		"tracks":       tracks,
	})
	if err == nil {
		t.ServiceID = serviceID
		t.ServiceName = serviceName
		t.Priority = priority
		t.CounterID = ""
		t.CounterName = ""
	}
	return err
}

func (t *Ticket) MovePreserveTime(services []string, counters []string) error {
	var tracks = t.Tracks
	var last = &tracks[len(tracks)-1]
	last.Counters = counters
	last.Services = services
	t.Services = services
	t.Counters = counters
	return ticketTable.UpdateID(t.ID, bson.M{
		"counters": counters,
		"services": services,
		"tracks":   tracks,
	})
}

func (t *Ticket) Cancel(userID string, counterID string, serviceID string) (err error) {
	if err := t.updateState(TicketStateCancelled, userID, counterID, serviceID, nil, nil); err != nil {
		return err
	}
	return nil
}

func (t *Ticket) Miss() (err error) {
	return t.updateState(TicketStateMissed, "", "", "", nil, nil)
}

func (t *Ticket) Create() error {
	t.State = TicketStateWaiting
	t.CTime = time.Now().Unix()
	t.MTime = t.CTime
	t.Counters = []string{}
	var track = Track{
		MTime:    t.CTime,
		Services: t.Services,
		State:    t.State,
	}
	t.Tracks = []Track{track}
	return ticketTable.Create(t)
}

func GetTodayTicket(branchID string) ([]*Ticket, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	var tickets []*Ticket
	return tickets, ticketTable.FindWhere(bson.M{
		"branch_id":  branchID,
		"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
		"$and": []bson.M{
			bson.M{
				"state": bson.M{"$ne": TicketStateFinished},
			},
		},
		"updated_at": bson.M{
			"$ne": 0,
		},
	}, &tickets, &options.FindOptions{
		Sort: map[string]interface{}{
			"priority":   -1,
			"created_at": 1,
		},
	})
}

// func RemoveAll() error {
// 	return ticketTable.RemoveAll(bson.M{})
// }

func CountMorningTicket(serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return ticketTable.CountDocuments(nil, bson.M{
		"service_id": serviceID,
		"$and": []bson.M{
			// bson.M{
			// 	"state": bson.M{"$eq": TicketStateFinished},
			// },
			bson.M{
				"created_at": bson.M{"$lte": now.BeginningOfMidDay().Unix()},
			},
			bson.M{
				"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
			},
		},
	})
}

func CountTodayTicketByService(serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return CountTicketBetweenByService(now.BeginningOfDay().Unix(), now.EndOfDay().Unix(), serviceID)
}

func CountTicketFromResetTimeByService(serviceID string) (int64, error) {
	var resetTime = &timer.Now{
		Time: now.MustParse(resetTime),
	}
	var nowTime = time.Now()
	if resetTime.Unix() > nowTime.Unix() {
		return CountTicketBetweenByService(now.BeginningOfDay().Unix(), now.EndOfDay().Unix(), serviceID)
	}
	return CountTicketBetweenByService(resetTime.BeginningOfMinute().Unix(), now.EndOfDay().Unix(), serviceID)
}

func CountTicketFromResetTimeByService2(serviceID string, nextService string) (int64, error) {
	var resetTime = &timer.Now{
		Time: now.MustParse(resetTime),
	}
	var nowTime = time.Now()
	if resetTime.Unix() > nowTime.Unix() {
		return CountTicketBetweenByService2(now.BeginningOfDay().Unix(), now.EndOfDay().Unix(), serviceID, nextService)
	}
	return CountTicketBetweenByService2(resetTime.BeginningOfMinute().Unix(), now.EndOfDay().Unix(), serviceID, nextService)
}

var resetTime = viper.GetString("business.reset_time")

func CountTicketFromResetTime() (int64, error) {
	var resetTime = &timer.Now{
		Time: now.MustParse(resetTime),
	}
	var nowTime = time.Now()
	if resetTime.Unix() > nowTime.Unix() {
		return CountTicketBetween(now.BeginningOfDay().Unix(), now.EndOfDay().Unix())
	}
	return CountTicketBetween(resetTime.BeginningOfMinute().Unix(), now.EndOfDay().Unix())
}

func CountTodayTicket() (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return CountTicketBetween(now.BeginningOfDay().Unix(), now.EndOfDay().Unix())
}

func CountTicketBetween(start, end int64) (int64, error) {
	return ticketTable.CountDocuments(nil, bson.M{
		"$and": []bson.M{
			// bson.M{
			// 	"state": bson.M{"$eq": TicketStateFinished},
			// },
			bson.M{
				"created_at": bson.M{"$lte": end},
			},
			bson.M{
				"created_at": bson.M{"$gte": start},
			},
		},
	})
}

func CountTicketBetweenByService(start, end int64, service string) (int64, error) {
	return ticketTable.CountDocuments(nil, bson.M{
		"service_id": service,
		"$and": []bson.M{
			bson.M{
				"created_at": bson.M{"$lte": end},
			},
			bson.M{
				"created_at": bson.M{"$gte": start},
			},
		},
	})
}

func CountTicketBetweenByService2(start, end int64, service, autoNextService string) (int64, error) {
	return ticketTable.CountDocuments(nil, bson.M{
		"service_id": bson.M{
			"$in": []string{service, autoNextService},
		},
		"$and": []bson.M{
			bson.M{
				"created_at": bson.M{"$lte": end},
			},
			bson.M{
				"created_at": bson.M{"$gte": start},
			},
		},
	})
}

func CountPrintedTicket() (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return ticketTable.CountDocuments(nil, bson.M{
		"$and": []bson.M{
			// bson.M{
			// 	"state": bson.M{"$eq": TicketStateFinished},
			// },
			bson.M{
				"created_at": bson.M{"$lte": now.Unix()},
			},
			bson.M{
				"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
			},
		},
	})
}

func CountPrintedTicketByService(serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return ticketTable.CountDocuments(nil, bson.M{
		"service_id": serviceID,
		"$and": []bson.M{
			// bson.M{
			// 	"state": bson.M{"$eq": TicketStateFinished},
			// },
			bson.M{
				"created_at": bson.M{"$lte": now.Unix()},
			},
			bson.M{
				"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
			},
		},
	})
}

func CountWaitingTicket(storeID, serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return ticketTable.CountDocuments(nil, bson.M{
		"branch_id":  storeID,
		"service_id": serviceID,
		"state":      TicketStateWaiting,
		"$and": []bson.M{
			// bson.M{
			// 	"state": bson.M{"$eq": TicketStateFinished},
			// },
			bson.M{
				"created_at": bson.M{"$lte": now.Unix()},
			},
			bson.M{
				"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
			},
		},
	})
}

func CountDailyTicket(timeNow int64, serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	if timeNow < now.BeginningOfMidDay().Unix() {
		return CountMorningTicket(serviceID)
	}
	return CountAfternoonTicket(serviceID)
}

func CountAfternoonTicket(serviceID string) (int64, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	return ticketTable.CountDocuments(nil, bson.M{
		"service_id": serviceID,
		"$and": []bson.M{
			bson.M{
				"created_at": bson.M{"$gte": now.BeginningOfMidDay().Unix()},
			},
			bson.M{
				"created_at": bson.M{"$lte": now.EndOfDay().Unix()},
			},
		},
	})
}

func GetTodayTicketByState(branchID string, states []string) ([]*Ticket, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	var tickets []*Ticket
	return tickets, ticketTable.FindWhere(bson.M{
		"branch_id":  branchID,
		"state":      bson.M{"$in": states},
		"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
	}, &tickets)
}

func GetTodayServingTicket(branchID string) ([]*Ticket, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	var tickets []*Ticket
	return tickets, ticketTable.FindWhere(bson.M{
		"branch_id":  branchID,
		"state":      TicketStateServing,
		"created_at": bson.M{"$gte": now.BeginningOfDay().Unix()},
	}, &tickets)
}

type TicketCount struct {
	ServiceID string `bson:"service_id" json:"service_id"`
	BranchID  string `bson:"branch_id" json:"branch_id"`
	// KioskID   string `bson:"kiosk_id" json:"kiosk_id"`
	Count int `bson:"count" json:"count"`
}

func GetTodayTicketCount() ([]*TicketCount, error) {
	var now = &timer.Now{
		Time: time.Now(),
	}
	var tickets []*TicketCount
	var group = bson.M{}
	var project = bson.M{}
	if viper.GetBool("business.tnumber_system") {
		group = bson.M{
			"$group": bson.M{
				"_id":   bson.M{"branch_id": "$branch_id", "kiosk_id": "$kiosk_id"},
				"count": bson.M{"$sum": 1},
			},
		}
		project = bson.M{
			"$project": bson.M{
				"_id":       0,
				"branch_id": "$_id.branch_id",
				// "kiosk_id":   "$_id.kiosk_id",
				"count": "$count",
			},
		}
	} else {
		group = bson.M{
			"$group": bson.M{
				"_id":   bson.M{"service_id": "$service_id", "branch_id": "$branch_id", "kiosk_id": "$kiosk_id"},
				"count": bson.M{"$sum": 1},
			},
		}
		project = bson.M{
			"$project": bson.M{
				"_id":        0,
				"service_id": "$_id.service_id",
				"branch_id":  "$_id.branch_id",
				// "kiosk_id":   "$_id.kiosk_id",
				"count": "$count",
			},
		}
	}
	return tickets, ticketTable.SimpleAggregate([]bson.M{
		bson.M{
			"$match": bson.M{
				"ctime": bson.M{"$gte": now.BeginningOfDay().Unix()},
			},
		},
		group,
		project,
	}, &tickets)
}

// func RemoveTodayTicket() error {
// 	var now = &timer.Now{
// 		Time: time.Now(),
// 	}
// 	return ticketTable.RemoveAll(bson.M{
// 		"ctime": bson.M{"$gte": now.BeginningOfDay().Unix()},
// 	})
// }
