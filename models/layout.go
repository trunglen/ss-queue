package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type Layout struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string `bson:"name" json:"name"`
	Type                 string `bson:"type" json:"type"`
	Logo                 string `bson:"logo" json:"logo"`
	BackgroundColor      string `bson:"background_color,omitempty" json:"background_color"`
	BackgroundPanel      string `bson:"background_panel,omitempty" json:"background_panel"`
	TextColor            string `bson:"text_color,omitempty" json:"text_color"`
	RightSidebar         string `bson:"right_sidebar,omitempty" json:"right_sidebar"`
	Template             string `bson:"template" json:"template"`
}

func (layout *Layout) Create() error {
	return layoutTable.Create(layout)
}

func GetAllLayouts(types string) ([]*Layout, error) {
	var layouts []*Layout
	if types == "" {
		return layouts, layoutTable.FindWhere(bson.M{}, &layouts)
	}
	return GetLayoutByType(types)
}

func (t *Layout) Update() error {
	return layoutTable.UpdateID(t.ID, bson.M{
		"name":             t.Name,
		"logo":             t.Logo,
		"background_color": t.BackgroundColor,
		"text_color":       t.TextColor,
		"background_panel": t.BackgroundPanel,
		"template":         t.Template,
		"right_sidebar":    t.RightSidebar,
	})
}

func DeleteLayoutByID(id string) error {
	return layoutTable.DeleteByID(id)
}

func GetLayoutByID(id string) (*Layout, error) {
	var layout *Layout
	return layout, layoutTable.FindByID(id, layout)
}

func GetLayoutByType(types string) ([]*Layout, error) {
	var layouts = []*Layout{}
	return layouts, layoutTable.FindWhere(bson.M{"type": types}, &layouts)
}
