package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type Kiosk struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string   `bson:"name" json:"name"`
	BranchID             string   `bson:"branch_id" json:"branch_id"`
	LayoutID             string   `bson:"layout_id" json:"layout_id"`
	Services             []string `bson:"services" json:"services,omitempty"`
	TLayoutID            string   `bson:"tlayout_id" json:"tlayout_id"`
	CustomerID           string   `bson:"customer_id" json:"customer_id"`
}

func GetAllKiosks() ([]*Kiosk, error) {
	var kiosks = []*Kiosk{}
	return kiosks, kioskTable.FindWhere(bson.M{}, &kiosks)
}

func (k *Kiosk) Create() error {
	return kioskTable.Create(k)
}

func (k *Kiosk) Update() error {
	return kioskTable.UpdateID(k.ID, bson.M{
		"services":   k.Services,
		"branch_id":  k.BranchID,
		"layout_id":  k.LayoutID,
		"name":       k.Name,
		"tlayout_id": k.TLayoutID,
	})
}

func DeleteKioskByID(id string) error {
	return kioskTable.DeleteByID(id)
}

func GetKiosksByServiceID(serviceID string) ([]*Kiosk, error) {
	var kiosks []*Kiosk
	return kiosks, kioskTable.FindWhere(bson.M{
		"services": bson.M{
			"$elemMatch": bson.M{
				"$eq": serviceID,
			},
		},
	}, &kiosks)
}

type KioskInfo struct {
	ID       string    `bson:"_id" json:"id"`
	BranchID string    `bson:"branch_id" json:"branch_id"`
	Services []Service `bson:"services" json:"services"`
	// TLayouts []TLayout `bson:"tlayout" json:"tlayout"`
	// Layouts  []Layout  `bson:"layout" json:"layout"`
}

func GetKioskInfo(kioskID string) (*KioskInfo, error) {
	var kioskInfo *KioskInfo
	return kioskInfo, kioskTable.SimpleAggregate(&kioskInfo, []bson.M{
		{
			"$match": bson.M{"_id": kioskID},
		},
		{
			"$lookup": bson.M{
				"from":         "service",
				"localField":   "services",
				"foreignField": "_id",
				"as":           "services",
			},
		},
		{
			"$lookup": bson.M{
				"from":         "tlayout",
				"localField":   "tlayout_id",
				"foreignField": "_id",
				"as":           "tlayout",
			},
		},
		{
			"$lookup": bson.M{
				"from":         "layout",
				"localField":   "layout_id",
				"foreignField": "_id",
				"as":           "layout",
			},
		},
	})
}

func GetKioskByLayoutID(layoutID string) ([]*Kiosk, error) {
	var kiosks = []*Kiosk{}
	return kiosks, kioskTable.FindWhere(bson.M{"layout_id": layoutID}, &kiosks)
}

func GetKioskByTLayoutID(tlayoutID string) ([]*Kiosk, error) {
	var kiosks = []*Kiosk{}
	return kiosks, kioskTable.FindWhere(bson.M{"tlayout_id": tlayoutID}, &kiosks)
}
