package models

import (
	"sort"
	"time"

	"gitlab.com/trunglen/ss-queue/api/common"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb/util/timer"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ObjectSearch struct {
	BranchIDs  []string
	ServiceIDs []string
	TimeStart  int64
	TimeEnd    int64
	WaitStart  int
	WaitEnd    int
}

type TicketReport struct {
	Ticket       `bson:",inline"`
	BranchName   string `bson:"branch_name" json:"branch_name"`
	BranchParent string `bson:"branch_parent" json:"branch_parent"`
}

func GetTickets() ([]*Ticket, error) {
	var tickets []*Ticket
	return tickets, ticketTable.FindWhere(bson.M{}, &tickets, &options.FindOptions{
		Sort: map[string]interface{}{
			"created_at": -1,
		},
	})
}
func GetTicketHistory(dataSearch ObjectSearch) ([]*TicketReport, error) {
	var tickets []*TicketReport
	var query = []bson.M{}
	var queryMatch = bson.M{
		"branch_id": bson.M{"$in": dataSearch.BranchIDs},
		"created_at": bson.M{
			"$gte": dataSearch.TimeStart,
			"$lte": dataSearch.TimeEnd,
		},
	}
	if dataSearch.ServiceIDs != nil && len(dataSearch.ServiceIDs) > 0 {
		queryMatch["service_id"] = bson.M{"$in": dataSearch.ServiceIDs}
	}
	if dataSearch.WaitStart > 0 && dataSearch.WaitEnd > 0 {
		queryMatch["nwaiting"] = bson.M{
			"$gte": dataSearch.WaitStart,
			"$lte": dataSearch.WaitEnd,
		}
	}

	var joinBranch = bson.M{
		"from":         "branch",
		"localField":   "branch_id",
		"foreignField": "_id",
		"as":           "branch",
	}
	var unWindBranch = bson.M{"path": "$branch", "preserveNullAndEmptyArrays": true}
	var project = bson.M{
		"created_at":     1,
		"updated_at":     1,
		"transaction_id": 1,
		"branch_id":      1,
		"kiosk_id":       1,
		"cnum":           1,
		"tnum":           1,
		"tracks":         1,
		"online":         1,
		"nwaiting":       1,
		"state":          1,
		"branch_name":    "$branch.name",
		"branch_parent":  "$branch.parent",
		"branch":         1,
	}
	var joinBranchParent = bson.M{
		"from":         "branch",
		"localField":   "branch_parent",
		"foreignField": "_id",
		"as":           "branch_new",
	}
	var unWindBranchNew = bson.M{"path": "$branch_new", "preserveNullAndEmptyArrays": true}
	var projectNew = bson.M{
		"created_at":     1,
		"updated_at":     1,
		"transaction_id": 1,
		"branch_id":      1,
		"kiosk_id":       1,
		"cnum":           1,
		"tnum":           1,
		"tracks":         1,
		"online":         1,
		"nwaiting":       1,
		"state":          1,
		"branch_name":    1,
		"branch_parent":  "$branch_new.name",
	}
	query = []bson.M{
		{"$match": queryMatch},
		{"$lookup": joinBranch},
		{"$unwind": unWindBranch},
		{"$project": project},
		{"$lookup": joinBranchParent},
		{"$unwind": unWindBranchNew},
		{"$project": projectNew},
	}
	return tickets, ticketTable.SimpleAggregate(query, &tickets)
}

func GetTicketGroupStatus(dataSearch ObjectSearch) ([]*TicketGroupStatus, error) {
	var tickets []*TicketGroupStatus
	var query = []bson.M{}
	var queryMatch = bson.M{
		"branch_id": bson.M{"$in": dataSearch.BranchIDs},
		"created_at": bson.M{
			"$gte": dataSearch.TimeStart,
			"$lte": dataSearch.TimeEnd,
		},
	}
	var group = bson.M{
		"_id":     "$state",
		"tickets": bson.M{"$push": "$$ROOT"},
		"count":   bson.M{"$sum": 1},
	}
	query = []bson.M{
		{"$match": queryMatch},
		{"$group": group},
	}
	return tickets, ticketTable.SimpleAggregate(query, &tickets)
}

func GetTicketGroupService(dataSearch ObjectSearch) ([]*TicketGroupStatus, error) {
	var tickets []*TicketGroupStatus
	var query = []bson.M{}
	var queryMatch = bson.M{
		"branch_id": bson.M{"$in": dataSearch.BranchIDs},
		"created_at": bson.M{
			"$gte": dataSearch.TimeStart,
			"$lte": dataSearch.TimeEnd,
		},
		//"state": bson.M{"$in": []TicketState{TicketStateCancelled, TicketStateFinished}},
	}
	var group = bson.M{
		"_id":     "$service_id",
		"tickets": bson.M{"$push": "$$ROOT"},
		"count":   bson.M{"$sum": 1},
	}
	var joinService = bson.M{
		"from":         "service",
		"localField":   "_id",
		"foreignField": "_id",
		"as":           "service",
	}
	var projectNew = bson.M{
		"_id":     1,
		"tickets": 1,
		"count":   1,
		"service": 1,
		// "service_name": bson.M{
		// 	"$arrayElemAt": []interface{}{"$ser.name", 0},
		// },
	}

	query = []bson.M{
		{"$match": queryMatch},
		{"$group": group},
		{"$lookup": joinService},
		{"$project": projectNew},
	}

	return tickets, ticketTable.SimpleAggregate(query, &tickets)
}

type TicketGroupStatus struct {
	ID          string    `bson:"_id" json:"_id"`
	Tickets     []*Ticket `bson:"tickets" json:"tickets"`
	Count       int       `bson:"count" json:"count"`
	ServiceName string    `bson:"service_name" json:"service_name"`
}

type ReportState struct {
	State                string  `bson:"state" json:"state"`
	Total                int     `bson:"total" json:"total"`
	TotalTime            int     `bson:"total_time" json:"total_time"`
	RatioCanel           float32 `bson:"ratio_cancel" json:"ratio_cancel"`
	RatioFinish          float32 `bson:"ratio_finish" json:"ratio_finish"`
	TotalFinish          int     `bson:"total_finish" json:"total_finish"`
	TotalCancel          int     `bson:"total_cancel" json:"total_cancel"`
	RatioStandartWait    float32 `bson:"ratio_wait_in" json:"ratio_wait_in"`
	RatioExceededWait    float32 `bson:"ratio_exceeded_wait" json:"ratio_exceeded_wait"`
	RatioStandartServing float32 `bson:"ratio_serving_in" json:"ratio_serving_in"`
	RatioExceededServing float32 `bson:"ratio_exceeded_serving" json:"ratio_exceeded_serving"`
	TotalTimeServing     int64   `bson:"total_time_serving" json:"total_time_serving"`
	TotalTimeWaiting     int64   `bson:"total_time_waiting" json:"total_time_waiting"`
	AverageServing       float32 `bson:"average_serving" json:"average_serving"`
	TimeServingBest      int64   `bson:"time_serving_best" json:"best_serving"`
	TimeServingSmallest  int64   `bson:"time_serving_smallest" json:"time_serving_smallest"`
	AverageWait          float32 `bson:"average_wait" json:"average_wait"`
	TimeWaitBest         int64   `bson:"time_wait_best" json:"time_wait_best"`
	TimeWaitSmallest     int64   `bson:"time_wait_smallest" json:"time_wait_smallest"`
}

type ReportService struct {
	ServiceID            string  `bson:"service_id" json:"service_id"`
	ServiceName          string  `bson:"service_name" json:"service_name"`
	Total                int     `bson:"total" json:"total"`
	TotalFinish          int     `bson:"total_finish" json:"total_finish"`
	TotalCancel          int     `bson:"total_cancel" json:"total_cancel"`
	RatioFinish          float32 `bson:"ratio_finish" json:"ratio_finish"`
	RatioCancel          float32 `bson:"ratio_cancel" json:"ratio_cancel"`
	TotalStandartWait    int     `bson:"total_standart_wait" json:"total_standart_wait"`
	RatioStandartoWait   float32 `bson:"ratio_standart_wait" json:"ratio_standart_wait"`
	TotalExceededWait    int     `bson:"total_exceeded_wait" json:"total_exceeded_wait"`
	RatioExceededWait    float32 `bson:"ratio_exceeded_wait" json:"ratio_exceeded_wait"`
	TotalStandartServing int     `bson:"total_standart_serving" json:"total_standart_serving"`
	RatioStandartServing float32 `bson:"ratio_standart_serving" json:"ratio_standart_serving"`
	TotalExceededServing int     `bson:"total_exceeded_serving" json:"total_exceeded_serving"`
	RatioExceededServing float32 `bson:"ratio_exceeded_serving" json:"ratio_exceede_serving"`
}

type Aggregate struct {
	TicketGroupStates   []*TicketGroupStatus `bson:"group_state" json:"group_state"`
	TicketGroupServices []*TicketGroupStatus `bson:"group_service" json:"group_service"`
	ReportState         *ReportState         `bson:"report_state" json:"report_state"`
	ReportServices      []*ReportService     `bson:"report_services" json:"report_services"`
}

func ReportAggregate(dataSearch ObjectSearch) (*Aggregate, error) {
	var tksGeneral, err = GetTicketGroupStatus(dataSearch)
	if err != nil {
		return nil, err
	}
	// var tkServices, err2 = GetTicketGroupService(dataSearch)
	// if err2 != nil {
	// 	return nil, err2
	// }
	var agg = &Aggregate{
		//	TicketGroupServices: tkServices,
		TicketGroupStates: tksGeneral,
		ReportState:       logicReportStatus(tksGeneral),
		//	ReportServices:      logicReportService(tkServices),
	}
	return agg, nil
}

func logicReportService(tkReportSers []*TicketGroupStatus) []*ReportService {
	var reSers = make([]*ReportService, len(tkReportSers))
	for i, report := range tkReportSers {
		var reSer = &ReportService{
			ServiceID:   report.ID,
			ServiceName: report.ServiceName,
			Total:       report.Count,
		}
		var countFinish = 0
		var countCancel = 0
		for _, tk := range report.Tickets {
			if tk.State == common.TicketActionCancel {
				countCancel++
			} else if tk.State == common.TicketActionFinish {
				countFinish++
			}
		}
		reSer.TotalFinish = countFinish
		reSer.TotalCancel = countCancel
		reSer.RatioCancel = float32(countCancel) / float32(report.Count)
		reSer.RatioFinish = float32(countFinish) / float32(report.Count)

		reSers[i] = reSer
	}
	return reSers
}

func logicReportStatus(tkReportSers []*TicketGroupStatus) *ReportState {
	var reStatus = &ReportState{}
	var countAll = 0
	var countCancel = 0
	var countFinish = 0

	var timeTotalServing int64
	var timeTotalWaiting int64

	var timeServingSmallest int64
	var timeServingBest int64
	var timeWaitSmallest int64
	var timeWaitBest int64

	for i, report := range tkReportSers {
		var timeServingNow int64
		var timeWaitingNow int64
		var timeFinishNow int64
		if report.ID == common.TicketActionCancel {
			countCancel += report.Count
		} else if report.ID == common.TicketActionFinish {
			countFinish += report.Count
		}
		countAll += report.Count
		for _, tk := range report.Tickets {
			var tracks = tk.Tracks
			sort.Sort(TrackList(tracks))
			for _, track := range tracks {
				if track.State == TicketStateWaiting {
					timeWaitingNow = track.MTime
				} else if track.State == TicketStateServing {
					timeServingNow = track.MTime
				} else if track.State == TicketStateFinished {
					timeFinishNow = track.MTime
				}
			}
		}
		var timeServing = timeFinishNow - timeServingNow
		var timeWait = timeServingNow - timeWaitingNow
		timeTotalServing += timeServing
		timeTotalWaiting += timeWait

		if i == 0 {
			timeServingSmallest = timeTotalWaiting
			timeServingBest = timeTotalServing
			timeWaitSmallest = timeTotalWaiting
			timeWaitBest = timeTotalWaiting
		} else {
			if timeServingSmallest > timeServing {
				timeServingSmallest = timeServing
			} else {
				timeServingBest = timeServing
			}

			if timeWaitSmallest > timeWait {
				timeWaitSmallest = timeWait
			} else {
				timeWaitBest = timeWait
			}

		}
	}
	reStatus.Total = countAll
	reStatus.RatioCanel = float32(countCancel) / float32(countAll)
	reStatus.RatioFinish = float32(countFinish) / float32(countAll)
	reStatus.TotalCancel = countCancel
	reStatus.TotalFinish = countFinish
	reStatus.TotalTimeServing = timeTotalServing
	reStatus.TotalTimeWaiting = timeTotalWaiting
	reStatus.TimeServingBest = timeServingBest
	reStatus.TimeServingSmallest = timeServingSmallest
	reStatus.TimeWaitBest = timeWaitBest
	reStatus.TimeWaitSmallest = timeWaitSmallest
	reStatus.AverageServing = float32(timeTotalServing) / float32(countAll)
	reStatus.AverageWait = float32(timeTotalWaiting) / float32(countAll)
	return reStatus
}

type GeneralReport struct {
	Day           string  `json:"day" bson:"_id"`
	TCount        int     `json:"tcount" bson:"tcount"`
	FTCount       int     `json:"ftcount" bson:"ftcount"`
	CTCount       int     `json:"ctcount" bson:"ctcount"`
	AvgSTime      float64 `json:"avg_stime" bson:"avg_stime"`
	MaxSTime      int     `json:"max_stime" bson:"max_stime"`
	AvgWTime      float64 `json:"avg_wtime" bson:"avg_wtime"`
	MaxWTime      int     `json:"max_wtime" bson:"max_wtime"`
	NoRateCount   int     `json:"no_rate_count" bson:"no_rate_count"`
	VeryGoodCount int     `json:"very_good_count" bson:"very_good_count"`
	GoodCount     int     `json:"good_count" bson:"good_count"`
	FairCount     int     `json:"fair_count" bson:"fair_count"`
	PoorCount     int     `json:"poor_count" bson:"poor_count"`
}

func GetGeneralReportLast7Days() ([]*GeneralReport, error) {
	var generalReport = []*GeneralReport{}
	cond := func(field string, op string, value interface{}, vIf interface{}, vElse interface{}) bson.M {
		return bson.M{
			"$cond": bson.M{
				"if":   bson.M{op: []interface{}{field, value}},
				"then": vIf,
				"else": vElse,
			},
		}
	}

	sum := func(value interface{}) bson.M {
		return bson.M{"$sum": value}
	}

	countIf := func(field string, op string, value interface{}) bson.M {
		return sum(cond(field, op, value, 1, 0))
	}

	avg := func(field string) bson.M {
		return bson.M{
			"$avg": field,
		}
	}

	max := func(field string) bson.M {
		return bson.M{
			"$max": field,
		}
	}

	group := bson.M{
		"$group": bson.M{
			"_id": bson.M{
				"$dateToString": bson.M{
					"format": "%Y-%m-%d",
					"date": bson.M{
						"$add": []interface{}{
							time.Date(1970, 01, 01, 0, 0, 0, 0, time.Local),
							bson.M{"$multiply": []interface{}{1000, "$created_at"}},
						},
					},
				},
			},
			"tcount":    sum(1),
			"ctcount":   countIf("$state", "$eq", TicketStateCancelled),
			"ftcount":   countIf("$state", "$eq", TicketStateFinished),
			"avg_stime": avg("$stime"),
			"max_stime": max("$stime"),
			"avg_wtime": avg("$wtime"),
			"max_wtime": max("$wtime"),
		},
	}

	var now = time.Now()
	var last7Days = time.Now().AddDate(0, -100, 0)
	var last7DaysTimer = timer.Now{
		Time: last7Days,
	}
	var match = bson.M{
		"$match": bson.M{
			"$and": []bson.M{
				// bson.M{"$gt": []interface{}{"$created_at", last7DaysTimer.BeginningOfDay().Unix()}},
				// bson.M{"$lte": []interface{}{"$created_at", now.Unix()}},
				bson.M{
					"created_at": bson.M{"$lte": now.Unix()},
				},
				bson.M{
					"created_at": bson.M{"$gte": last7DaysTimer.BeginningOfDay().Unix()},
				},
			},
		},
	}
	return generalReport, ticketTable.SimpleAggregate(&generalReport, match, group)
}

func GetGeneralReport() (*GeneralReport, error) {
	var generalReport = GeneralReport{}
	cond := func(field string, op string, value interface{}, vIf interface{}, vElse interface{}) bson.M {
		return bson.M{
			"$cond": bson.M{
				"if":   bson.M{op: []interface{}{field, value}},
				"then": vIf,
				"else": vElse,
			},
		}
	}

	sum := func(value interface{}) bson.M {
		return bson.M{"$sum": value}
	}

	countIf := func(field string, op string, value interface{}) bson.M {
		return sum(cond(field, op, value, 1, 0))
	}

	avg := func(field string) bson.M {
		return bson.M{
			"$avg": field,
		}
	}

	max := func(field string) bson.M {
		return bson.M{
			"$max": field,
		}
	}

	group := bson.M{
		"$group": bson.M{
			"_id":             "_id",
			"tcount":          sum(1),
			"ctcount":         countIf("$state", "$eq", TicketStateCancelled),
			"ftcount":         countIf("$state", "$eq", TicketStateFinished),
			"very_good_count": countIf("$rating", "$eq", 10),
			"good_count":      countIf("$rating", "$eq", 8),
			"fair_count":      countIf("$rating", "$eq", 6),
			"poor_count":      countIf("$rating", "$eq", 4),
			"no_rate_count":   countIf("$rating", "$eq", 0),
			"avg_stime":       avg("$stime"),
			"max_stime":       max("$stime"),
			"avg_wtime":       avg("$wtime"),
			"max_wtime":       max("$wtime"),
		},
	}

	_, err := ticketTable.SimpleAggregateFirst(&generalReport, group)

	// if generalReport == nil {
	// 	return &GeneralReport{}, nil
	// }
	return &generalReport, err
}
