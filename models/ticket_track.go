package models

import (
	"time"

	"github.com/fatih/structs"
	"github.com/trunglen/g/x/web"
)

type Track struct {
	UserID    string      `bson:"user_id,omitempty" json:"user_id,omitempty"`
	CounterID string      `bson:"counter_id,omitempty" json:"counter_id,omitempty"`
	State     TicketState `bson:"state" json:"state"`
	MTime     int64       `bson:"mtime" json:"mtime"` //Time start
	ServiceID string      `bson:"service_id,omitempty" json:"service_id,omitempty"`
	Services  []string    `bson:"services,omitempty" json:"services,omitempty"`
	Counters  []string    `bson:"counters,omitempty" json:"counters,omitempty"`
	Standard  int32       `bson:"standard,omitempty" json:"standard,omitempty"`
}

type TrackList []Track

func (t TrackList) GetLast() *Track {
	return &t[len(t)-1]
}

func (t *TrackList) Append(track Track) {
	*t = append(*t, track)
}

func (t TrackList) TransferCount() int {

	if len(t) > 3 {
		return 1
	}
	return 0
}

func (t TrackList) prevTrack() *Track {
	var n = len(t)
	if n < 2 {
		return nil
	}
	return &t[n-2]
}

func (t *Track) isFinished() bool {
	return t.State == TicketStateFinished
}

func (t *Ticket) updateState(state TicketState, userID, counterID, serviceID string, services, counters []string) error {
	var track = Track{
		UserID:    userID,
		CounterID: counterID,
		ServiceID: serviceID,
		State:     state,
		MTime:     time.Now().Unix(),
	}

	var back = t.Tracks[len(t.Tracks)-1]
	track.MTime = time.Now().Unix()
	t.State = track.State
	t.MTime = time.Now().Unix()
	t.UserID = track.UserID
	t.ServiceID = track.ServiceID
	t.CounterID = track.CounterID

	if services != nil {
		track.Services = services
		t.Services = services
	}

	if counters != nil {
		track.Counters = counters
		t.Counters = counters
	}

	t.Tracks = append(t.Tracks, track)

	var err = ticketTable.UpdateID(t.ID, structs.Map(t))

	if err != nil {
		t.State = back.State
		t.MTime = back.MTime
		t.UserID = back.UserID
		t.ServiceID = back.ServiceID
		t.CounterID = back.CounterID
		t.Counters = back.Counters
		t.Services = back.Services
		return web.InternalServerError("update ticket fail")
	}
	return nil
}
func (a TrackList) Len() int           { return len(a) }
func (a TrackList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a TrackList) Less(i, j int) bool { return a[i].MTime < a[j].MTime }
