package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type Media struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string `bson:"name" json:"name"`
	Type                 string `bson:"type" json:"type"`
	Ext                  string `bson:"ext" json:"ext"`
	Title                string `bson:"title" json:"title"`
	Path                 string `bson:"path" json:"path"`
	Size                 string `bson:"size" json:"size"`
	GUID                 string `bson:"guid" json:"guid"`
}

func (m *Media) Create() error {
	return mediaTable.Create(m)
}

func GetAllMedias() ([]*Media, error) {
	var medias = []*Media{}
	return medias, mediaTable.FindWhereNewest(bson.M{}, &medias)
}

func GetMediaByID(id string) (*Media, error) {
	var medias = &Media{}
	return medias, mediaTable.FindByID(id, medias)
}

func DeleteMediaByID(id string) error {
	return mediaTable.DeleteByID(id)
}
