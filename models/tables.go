package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
)

//CounterTable table of counter
var counterTable = mongodb.NewCollection("counter")
var branchTable = mongodb.NewCollection("branch")
var customerTable = mongodb.NewCollection("customer")
var scustomerTable = mongodb.NewCollection("scustomer")
var userTable = mongodb.NewCollection("user")
var kioskTable = mongodb.NewCollection("kiosk")
var serviceTable = mongodb.NewCollection("service")
var ticketTable = mongodb.NewCollection("ticket")
var tlayoutTable = mongodb.NewCollection("tlayout")
var layoutTable = mongodb.NewCollection("layout")
var transactionTable = mongodb.NewCollection("transaction")
var feedbackTable = mongodb.NewCollection("feedback")
var screenTable = mongodb.NewCollection("screen")
var mediaTable = mongodb.NewCollection("media")
