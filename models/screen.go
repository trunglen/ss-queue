package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	VIDEO_ADS_TYPE = "video"
	IMAGE_ADS_TYPE = "image"
)

type Advertisement struct {
	Link string `bson:"link" json:"link"`
	Type string `bson:"type" json:"type"`
}

type Screen struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string          `bson:"name" json:"name"`
	BranchID             string          `bson:"branch_id" json:"branch_id"`
	Advertisements       []Advertisement `bson:"advertisements" json:"advertisements"`
	LayoutID             string          `bson:"layout_id" json:"layout_id"`
	AdvertisementType    string          `bson:"advertisement_type" json:"advertisement_type"`
}

func (s *Screen) Update() error {
	return screenTable.UpdateID(s.ID, bson.M{
		"branch_id":          s.BranchID,
		"name":               s.Name,
		"advertisements":     s.Advertisements,
		"advertisement_type": s.AdvertisementType,
		"layout_id":          s.LayoutID,
	})
}

func (s *Screen) Create() error {
	return screenTable.Create(s)
}

func GetAllScreens() ([]*Screen, error) {
	var screens []*Screen
	return screens, screenTable.FindWhere(bson.M{}, &screens)
}

func DeleteScreenByID(id string) error {
	return screenTable.DeleteByID(id)
}

type ScreenInfo struct {
	ID      string   `bson:"_id" json:"id"`
	Layouts []Layout `bson:"layout" json:"layout"`
}

func GetScreenLayout(screenID string) (*ScreenInfo, error) {
	var screenInfo *ScreenInfo
	return screenInfo, screenTable.SimpleAggregate(&screenInfo, []bson.M{
		bson.M{
			"$match": bson.M{"_id": screenID},
		},
		bson.M{
			"$lookup": bson.M{
				"from":         "layout",
				"localField":   "layout_id",
				"foreignField": "_id",
				"as":           "layout",
			},
		},
	})
}

func GetScreenByLayoutID(layoutID string) ([]*Screen, error) {
	var screens []*Screen
	return screens, screenTable.FindWhere(bson.M{"layout_id": layoutID}, &screens)
}
