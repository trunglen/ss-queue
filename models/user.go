package models

import (
	"strings"

	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	errUserExist      = rest.BadRequest("Tài khoản đã tồn tại!")
	errUserWrongLogin = rest.BadRequest("Sai tên đăng nhập hoặc mật khẩu")
	errUnameInvalid   = rest.BadRequest("Tên đăng nhập đã tồn tại")
	userTagLog        = "UserTable"
)

type User struct {
	mongodb.DefaultModel `bson:",inline"`
	Username             string `bson:"username" json:"username,omitempty"`
	Name                 string `bson:"name" json:"name"`
	Password             string `bson:"password" json:"password,omitempty"`
	// HashedPassword    string `bson:"password" json:"-"`
	// Password          Password `bson:"-" json:"password"`
	Email    string `bson:"email" json:"email,omitempty"`
	BranchID string `bson:"branch_id" json:"branch_id"`
	Role     string `bson:"role" json:"role"`
}

func (u *User) Create() error {
	if err := u.validate(); err != nil {
		return err
	}

	if err := u.ensureUniqueUsername(); err != nil {
		return err
	}
	var p = password(u.Password)
	// replace
	if err := p.HashTo(&u.Password); err != nil {
		return err
	}
	return userTable.Create(u)
}

func (u *User) Update() error {
	var p = password(u.Password)
	if err := p.HashTo(&u.Password); err != nil {
		return err
	}
	return userTable.UpdateID(u.ID, bson.M{
		"username": u.Username,
		"name":     u.Name,
		"password": u.Password,
	})
}

func (u *User) Login(email, pwd string) (*User, error) {
	var user, err = GetUserByUnameRole(email, "")
	if err != nil {
		return nil, err
	}
	if err = password(pwd).Compare(user.Password); err != nil {
		return nil, errUserWrongLogin
	}
	return user, nil
}

func DeleteUserByID(id string) error {
	return userTable.DeleteByID(id)
}

func GetAdmin(username string) (*User, error) {
	var u User
	return &u, userTable.FindSingle(map[string]interface{}{
		"username": username,
		"role":     RoleAdminRoot,
	}, &u)
}

func GetByUsernameBanchID(username string, branchID string) (*User, error) {
	var u *User
	return u, userTable.FindSingle(map[string]interface{}{
		"username":  username,
		"branch_id": branchID,
	}, &u)
}

func GetByBranch(branches []string) ([]*User, error) {
	var users = []*User{}
	return users, userTable.FindWhere(bson.M{
		"branch_id": bson.M{
			"$in": branches,
		},
	}, &users)
}

func GetUserByRole(role string) ([]*User, error) {
	var users = []*User{}
	return users, userTable.FindWhere(bson.M{
		"role": role,
	}, &users)
}

func GetUserByUnameRole(uname string, role Role) (*User, error) {
	var user *User
	return user, userTable.FindSingle(bson.M{
		"role":     role,
		"username": uname,
	}, &user)
}

func GetAllUserByAdmin() ([]*User, error) {
	var users = []*User{}
	return users, userTable.FindWhere(bson.M{
		"role": bson.M{
			"$ne": RoleAdminRoot,
		},
	}, &users)
}

func (u *User) ensureUniqueUsername() error {
	var user *User
	if len(u.Username) < 3 {
		return rest.BadRequest("user name length must be >3")
	}
	userTable.FindSingle(map[string]interface{}{
		"username": u.Username,
		"role":     u.Role,
	}, &user)

	if user != nil {
		return rest.BadRequest(errUserWrongLogin)
	}
	return nil
}

const errMissingBranchID = rest.BadRequest("missing branch_id")

func (u *User) validate() error {
	u.Username = strings.ToLower(strings.TrimSpace(u.Username))
	// if len(u.BranchID) < 1 {
	// 	return errMissingBranchID
	// }
	return nil
}
