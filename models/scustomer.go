package models

import (
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

const (
	ERR_EXIST_CUSTOMER_EMAIL = rest.BadRequest("Email đăng ký đã tồn tại. Vui lòng kiểm tra lại!")
	ERR_EXIST_CUSTOMER_PHONE = rest.BadRequest("Số điện thoại đăng ký đã tồn tại. Vui lòng kiểm tra lại!")
)

type SCustomer struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string `bson:"name" json:"name"`
	Email                string `bson:"email" json:"email"`
	Password             string `bson:"password" json:"password"`
	Phone                string `bson:"phone" json:"phone"`
	Logo                 string `bson:"logo" json:"logo"`
	Company              string `bson:"company" json:"company"`
	Address              string `bson:"address" json:"address"`
	Info                 string `bson:"info" json:"info"`
	RegisterCode         string `bson:"register_code" json:"register_code"`
	Activated            bool   `bson:"activated" json:"activated"`
}

type License struct {
	NumOfKiosks   int `bson:"num_of_kiosks" json:"num_of_kiosks"`
	NumOfCounters int `bson:"num_of_counters" json:"num_of_counters"`
	NumOfScreens  int `bson:"num_of_screens" json:"num_of_screens"`
	NumOfFeedback int `bson:"num_of_feedback" json:"num_of_feedback"`
	ExpiredAt     int `bson:"expired_at" json:"expired_at"`
}

func (c *SCustomer) Create() error {
	var findCustomer, _ = FindSCustomerByEmail(c.Email)
	if findCustomer != nil {
		return ERR_EXIST_CUSTOMER_EMAIL
	}

	findCustomer, _ = FindSCustomerByPhone(c.Phone)
	if findCustomer != nil {
		return ERR_EXIST_CUSTOMER_PHONE
	}

	var p = password(c.Password)
	if err := p.HashTo(&c.Password); err != nil {
		return err
	}

	return scustomerTable.Create(c)
}

func (c *SCustomer) Update() error {
	return scustomerTable.UpdateID(c.ID, bson.M{
		"name":    c.Name,
		"phone":   c.Phone,
		"company": c.Company,
		"address": c.Address,
	})
}

func (c *SCustomer) Activate() error {
	return scustomerTable.UpdateID(c.ID, bson.M{
		"activated": true,
	})
}

func (c *SCustomer) DeActivate() error {
	return scustomerTable.UpdateID(c.ID, bson.M{
		"activated": false,
	})
}

func (u *SCustomer) Login(email, pwd string) (*SCustomer, error) {
	var user, err = FindSCustomerByEmail(email)
	if err != nil {
		return nil, err
	}
	if err = password(pwd).Compare(user.Password); err != nil {
		return nil, errUserWrongLogin
	}
	return user, nil
}

func FindSCustomerByID(id string) (*SCustomer, error) {
	var c *SCustomer
	return c, scustomerTable.First(bson.M{"_id": id}, &c)
}

func FindSCustomerByEmail(email string) (*SCustomer, error) {
	var c *SCustomer
	return c, scustomerTable.First(bson.M{"email": email}, &c)
}

func FindSCustomerByPhone(phone string) (*SCustomer, error) {
	var c *SCustomer
	err := scustomerTable.First(bson.M{"phone": phone}, &c)
	return c, err
}

func FindAllSCustomer() ([]*SCustomer, error) {
	var customers []*SCustomer
	err := scustomerTable.FindWhereNewest(bson.M{}, &customers)
	return customers, err
}
