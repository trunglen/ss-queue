package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type Service struct {
	mongodb.DefaultModel   `bson:",inline"`
	Name                   string `bson:"name" json:"name"`
	Prefix                 string `bson:"prefix" json:"prefix"`
	Priority               int    `bson:"priority" json:"priority"`
	TicketLimited          int64  `bson:"ticket_limited,omitempty" json:"ticket_limited"`
	MorningTicketLimited   int64  `bson:"morning_ticket_limited,omitempty" json:"morning_ticket_limited"`
	AfternoonTicketLimited int64  `bson:"afternoon_ticket_limited,omitempty" json:"afternoon_ticket_limited"`
	StartNumber            int64  `bson:"start_number,omitempty" json:"start_number"`
	AutoNextService        string `bson:"auto_next_service,omitempty" json:"auto_next_service"`
}

func (s *Service) Create() error {
	return serviceTable.Create(s)
}

func (s *Service) Update() error {
	return serviceTable.UpdateID(s.ID, bson.M{
		"name":                     s.Name,
		"prefix":                   s.Prefix,
		"priority":                 s.Priority,
		"ticket_limited":           s.TicketLimited,
		"morning_ticket_limited":   s.MorningTicketLimited,
		"afternoon_ticket_limited": s.AfternoonTicketLimited,
		"start_number":             s.StartNumber,
		"auto_next_service":        s.AutoNextService,
	})
}

func GetAllServices() ([]*Service, error) {
	var services = []*Service{}
	return services, serviceTable.FindWhereNewest(bson.M{}, &services)
}

func DeleteServiceByID(id string) error {
	return serviceTable.DeleteByID(id)
}

func GetServicesByBranchID(branchID string) ([]*Service, error) {
	var services []*Service
	return services, serviceTable.FindWhere(bson.M{"branch_id": branchID}, &services)
}

func GetServiceByID(id string) (*Service, error) {
	var service *Service
	return service, serviceTable.FindByID(id, service)
}
