package models

import (
	"strconv"

	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Counter struct {
	mongodb.DefaultModel `bson:",inline"`
	BranchID             string   `bson:"branch_id" json:"branch_id"`
	Name                 string   `bson:"name" json:"name"`
	UserID               string   `bson:"user_id" json:"user_id,omitempty"`
	CNum                 int      `bson:"cnum" json:"cnum,omitempty"`
	LedAddress           int      `bson:"led_address" json:"led_address,omitempty"`
	Services             []string `bson:"services" json:"services,omitempty"`
	CallVoice            string   `bson:"call_voice" json:"call_voice"`
	PriorityServices     []string `bson:"priority_services" json:"priority_services,omitempty"`
	UseLCD               bool     `bson:"use_lcd" json:"use_lcd"`
	CustomerID           string   `bson:"customer_id" json:"customer_id"`
}

func (c *Counter) Create() error {
	return counterTable.Create(c)
}

func GetCounterByID(id string) (*Counter, error) {
	var counter *Counter
	return counter, counterTable.FindByID(id, counter)
}

func GetCountersByServiceID(serviceID string) ([]*Counter, error) {
	var counters []*Counter
	return counters, counterTable.FindWhere(bson.M{
		"services": bson.M{
			"$elemMatch": bson.M{
				"$eq": serviceID,
			},
		},
	}, &counters)
}

func GetCountersByBranchID(branchID string) ([]*Counter, error) {
	var counters []*Counter
	return counters, counterTable.FindWhere(bson.M{"branch_id": branchID}, &counters)
}

func (c *Counter) Update() error {
	return counterTable.UpdateID(c.ID, bson.M{
		"branch_id":         c.BranchID,
		"name":              c.Name,
		"cnum":              c.CNum,
		"services":          c.Services,
		"call_voice":        c.CallVoice,
		"priority_services": c.PriorityServices,
		"use_lcd":           c.UseLCD,
	})
}

func GetAllCounters() ([]*Counter, error) {
	var counter = []*Counter{}
	return counter, counterTable.FindWhereNewest(bson.M{}, &counter)
}

func GetServingCountersByService(serviceID string) string {
	var counter []*Counter
	var counterStr = ""
	counterTable.FindWhere(bson.M{
		"services": serviceID,
	}, &counter, &options.FindOptions{
		Sort: bson.M{
			"cnum": 1,
		},
	})
	if counter != nil {
		if len(counter) == 1 {
			counterStr = strconv.Itoa(counter[0].CNum)
		} else {
			for i, c := range counter {
				if i == 0 {
					counterStr = strconv.Itoa(c.CNum)
				} else {
					counterStr = counterStr + ", " + strconv.Itoa(c.CNum)
				}
			}
		}
	}
	return counterStr
}

// func CountLCD() int {
// 	result, _ := counterTable.CountWhere(bson.M{"use_lcd": true})
// 	return int(result)
// }

func DeleteCounterByID(id string) error {
	return counterTable.DeleteByID(id)
}
