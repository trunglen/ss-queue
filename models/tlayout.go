package models

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/bson"
)

type TLayout struct {
	mongodb.DefaultModel `bson:",inline"`
	Name                 string `bson:"name" json:"name"`
	Content              string `bson:"content" json:"content"`
}

func (tlayout *TLayout) Create() error {
	return tlayoutTable.Create(tlayout)
}

func GetAllTLayouts() ([]*TLayout, error) {
	var tlayouts = []*TLayout{}
	return tlayouts, tlayoutTable.FindWhereNewest(bson.M{}, &tlayouts)
}

func (t *TLayout) Update() error {
	return tlayoutTable.UpdateID(t.ID, bson.M{
		"name":    t.Name,
		"content": t.Content,
	})
}

func DeleteTLayoutByID(id string) error {
	return tlayoutTable.DeleteByID(id)
}
