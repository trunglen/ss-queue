package main

import (
	_ "gitlab.com/trunglen/ss-queue/config"
	"gitlab.com/trunglen/ss-queue/server"
)

func main() {
	server.Init()
}
