package builder

import (
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb/util"
	"gopkg.in/mgo.v2/bson"
)

// appendIfHasVal append key and val to map if value is not empty.
func appendIfHasVal(m bson.M, key string, val interface{}) {
	if !util.IsNil(val) {
		m[key] = val
	}
}
