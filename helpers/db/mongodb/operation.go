package mongodb

import (
	"context"

	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb/field"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func create(ctx context.Context, c *Collection, model Model, opts ...*options.InsertOneOptions) error {
	// Call to saving hook
	model.BeforeCreate()
	_, err := c.InsertOne(ctx, model, opts...)

	if err != nil {
		return err
	}
	return callToAfterCreateHooks(model)
}

func first(ctx context.Context, c *Collection, filter interface{}, result interface{}, opts ...*options.FindOneOptions) error {
	return c.FindOne(ctx, filter, opts...).Decode(result)
}

func update(ctx context.Context, c *Collection, model Model, opts ...*options.UpdateOptions) error {
	// Call to saving hook
	model.BeforeUpdate()
	res, err := c.UpdateOne(ctx, bson.M{field.ID: model.GetID()}, bson.M{"$set": model}, opts...)

	if err != nil {
		return err
	}

	return callToAfterUpdateHooks(res, model)
}

func del(ctx context.Context, c *Collection, id string) error {
	_, err := c.UpdateOne(ctx, bson.M{field.ID: id}, bson.M{"$set": bson.M{
		"updated_at": 0,
	}})
	return err
}
