package timer

import "fmt"

func secondsToTime(seconds int64) string {
	s := (seconds % 3600) % 60
	m := (seconds % 3600) / 60
	h := seconds / 3600
	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
}
