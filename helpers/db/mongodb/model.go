package mongodb

import (
	"time"

	"github.com/trunglen/g/x/math"
)

// CollectionGetter interface contain method to return
// model's custom collection.
type CollectionGetter interface {
	// Collection method return collection
	Collection() *Collection
}

// CollectionNameGetter interface contain method to return
// collection name of model.
type CollectionNameGetter interface {
	// CollectionName method return model collection's name.
	CollectionName() string
}

// Model interface is base method that must implement by
// each model, If you're using `DefaultModel` struct in your model,
// don't need to implement any of those method.
type Model interface {
	// PrepareID convert id value if need, and then
	// return it.(e.g convert string to objectId)
	BeforeCreate()
	BeforeUpdate()
	BeforeDelete()
	GetID() string
}

// DefaultModel struct contain model's default fields.
type DefaultModel struct {
	ID        string `json:"id" bson:"_id"`
	CreatedAt int64  `json:"created_at" bson:"created_at"`
	UpdatedAt int64  `json:"updated_at" bson:"updated_at"`
}

func (b *DefaultModel) GetID() string {
	return b.ID
}

func (b *DefaultModel) BeforeCreate() {
	b.ID = math.RandStringNoPrefix(24)
	b.CreatedAt = time.Now().Unix()
	b.UpdatedAt = time.Now().Unix()
}

func (b *DefaultModel) BeforeUpdate() {
	b.UpdatedAt = time.Now().Unix()
}

func (b *DefaultModel) BeforeDelete() {
	b.UpdatedAt = 0
}
