package config

import (
	"gitlab.com/trunglen/ss-queue/helpers/logger"

	"github.com/spf13/viper"
	"gitlab.com/trunglen/ss-queue/helpers/db/mongodb"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConfigDatabase() {
	err := mongodb.SetDefaultConfig(nil, viper.GetString("mongo.db"), options.Client().ApplyURI(viper.GetString("mongo.path")))
	if err != nil { // Handle errors reading the config file
		logger.Log.Fatalf("Fatal error config database: %+v\n", err)
	}
	logger.Log.Info("Successfully connect to database")
}
