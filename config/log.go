package config

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/trunglen/ss-queue/helpers/logger"
)

func ConfigLog() {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{})
	log.SetReportCaller(true)
	logger.SetLogger(log)
}
