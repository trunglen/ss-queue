package common

const (
	ErrInvalidUnameOrPassword = "Sai tên đăng nhập hoặc mật khẩu"
	ErrExistUsername          = "Tên đăng nhập đã tồn tại"
	ErrUsernameLength         = "Tên đăng nhập phải lớn hơn 6 kí tự"
	ErrUnknownRole            = "Role không hợp lệ"
)

const (
	TicketActionCall        = "call"
	TicketActionMiss        = "miss"
	TicketActionCancel      = "cancel"
	TicketActionFinish      = "finish"
	TicketActionRestore     = "restore"
	TicketActionCreate      = "create"
	TicketActionRating      = "rating"
	TicketActionMove        = "move"
	TicketActionMoveService = "move_service"
	TicketActionFeedback    = "feedback"
	TicketActionRecall      = "recall"
	TicketActionRefresh     = "refresh"
	TicketActionWaitLong    = "wait_long"
	TicketActionServeLong   = "serve_long"
	TicketActionAutoFinish  = "auto_finish"
)

const (
	DEVICE byte = 1
)
