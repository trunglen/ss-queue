package scustomer

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"gitlab.com/trunglen/ss-queue/models"

	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
)

func (s *SCustomerApi) handleCreateScreen(c *gin.Context) {
	var screen *models.Screen
	rest.AssertNil(c.BindJSON(&screen))
	rest.AssertNil(screen.Create())
	s.SendData(c, screen)
}

func (s *SCustomerApi) handleGetVideos(c *gin.Context) {
	folder := "./upload/screen"
	var files = []string{}
	err := filepath.Walk(folder, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, fmt.Sprintf("http://%s/%s", c.Request.Host, strings.Replace(path, string(filepath.Separator), "/", -1)))
		}
		return nil
	})
	rest.AssertNil(err)

	for _, file := range files {
		fmt.Println(file)
	}
	sort.Strings(files)

	filesR, err := ioutil.ReadDir("./upload/screen")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range filesR {
		fmt.Println(file.Name())
	}
	s.SendData(c, files)
}

func (s *SCustomerApi) handleUpdateScreen(c *gin.Context) {
	var screen *models.Screen
	rest.AssertNil(c.BindJSON(&screen))
	rest.AssertNil(screen.Update())
	// go func() {
	// 	evb.Publish(screen.ID, evb.SCREEN_RELOAD_EVENT)
	// }()
	s.SendData(c, screen)
}

func (s *SCustomerApi) handleGetScreens(c *gin.Context) {
	var screens, err = models.GetAllScreens()
	rest.AssertNil(err)
	s.SendData(c, screens)
}

func (s *SCustomerApi) handleDeleteScreen(c *gin.Context) {
	var err = models.DeleteScreenByID(c.Query("id"))
	rest.AssertNil(err)
	s.Success(c)
}
