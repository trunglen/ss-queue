package scustomer

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *SCustomerApi) handleCreateLayout(c *gin.Context) {
	var layout *models.Layout
	rest.AssertNil(c.BindJSON(&layout))
	rest.AssertNil(layout.Create())
	s.SendData(c, layout)
}

func (s *SCustomerApi) handleUpdateLayouts(c *gin.Context) {
	var layout *models.Layout
	rest.AssertNil(c.BindJSON(&layout))
	go func() {
		if layout.Type == "kiosk" {
			var kiosks, _ = models.GetKioskByLayoutID(layout.ID)
			if kiosks != nil {
				// for _, kiosk := range kiosks {
				// 	// evb.Publish(kiosk.ID, evb.KIOSK_RELOAD_EVENT)
				// }
			}
		} else if layout.Type == "screen" {
			var screens, _ = models.GetScreenByLayoutID(layout.ID)
			if screens != nil {
				// for _, screen := range screens {
				// 	// evb.Publish(screen.ID, evb.SCREEN_RELOAD_EVENT)
				// }
			}
		}

	}()
	rest.AssertNil(layout.Update())
	s.SendData(c, layout)
}

func (s *SCustomerApi) handleDeleteLayouts(c *gin.Context) {
	var id = c.Query("id")
	rest.AssertNil(models.DeleteLayoutByID(id))
	s.Success(c)
}

func (s *SCustomerApi) handleGetLayouts(c *gin.Context) {
	var types = c.Query("type")
	var layouts, err = models.GetAllLayouts(types)
	rest.AssertNil(err)
	s.SendData(c, layouts)
}

func (s *SCustomerApi) uploadScreenLogo(c *gin.Context) {
	var file, err = c.FormFile("file")
	rest.AssertNil(err)
	var uploadFileName = "screen_logo"
	rest.AssertNil(c.SaveUploadedFile(file, "./upload"+uploadFileName))
	s.SendData(c, map[string]interface{}{
		"url": "/upload" + uploadFileName,
	})
}
