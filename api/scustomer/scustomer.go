package scustomer

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
)

type SCustomerApi struct {
	rest.JsonRender
}

func NewSCustomerApi(router *gin.RouterGroup) {
	var s = new(SCustomerApi)
	// router.Use(middlewares.AuthenticateAdmin().MiddlewareFunc())
	router.GET("report/history", s.handleHistory)
	router.GET("report/quantity/chart", s.handleQuantityChart)
	router.GET("report/general", s.handleGeneralReport)

	router.POST("counter/create", s.handleCreateCounter)
	router.POST("counter/update", s.handleUpdateCounter)
	router.GET("counter/list", s.handleListCounter)
	router.DELETE("counter/delete", s.handleDeleteCounter)

	router.POST("branch/create", s.handleCreateBranch)
	router.POST("branch/update", s.handleUpdateBranch)
	router.DELETE("branch/delete", s.handleDeleteBranch)
	router.GET("branch/list", s.handleGetBranches)

	router.POST("service/create", s.handleCreateService)
	router.POST("service/update", s.handleUpdateService)
	router.GET("service/list", s.handleGetServices)
	router.DELETE("service/delete", s.handleDeleteServices)

	router.POST("kiosk/create", s.handleCreateKiosk)
	router.GET("kiosk/list", s.handleGetKiosks)
	router.POST("kiosk/update", s.handleUpdateKiosk)
	router.DELETE("kiosk/delete", s.handleDeleteKiosk)

	router.POST("screen/create", s.handleCreateScreen)
	router.POST("screen/update", s.handleUpdateScreen)
	router.GET("screen/list", s.handleGetScreens)
	router.GET("screen/video/list", s.handleGetVideos)
	router.DELETE("screen/delete", s.handleDeleteScreen)

	router.POST("user/create", s.handleCreateUser)
	router.POST("user/update", s.handleUpdateUser)
	router.GET("user/list", s.handleGetUsers)
	router.DELETE("user/delete", s.handleDeleteUser)

	router.POST("layout/create", s.handleCreateLayout)
	router.POST("layout/update", s.handleUpdateLayouts)
	router.POST("layout/logo/upload", s.uploadScreenLogo)
	router.DELETE("layout/delete", s.handleDeleteLayouts)
	router.GET("layout/list", s.handleGetLayouts)

	router.POST("tlayout/create", s.handleCreateTLayout)
	router.POST("tlayout/update", s.handleUpdateTLayouts)
	router.DELETE("tlayout/delete", s.handleDeleteTLayouts)
	router.GET("tlayout/list", s.handleGetTLayouts)

	router.POST("media/create", s.handleCreateMedia)
	router.GET("media/list", s.handleGetMedias)
	router.DELETE("media/delete", s.handleDeleteMedia)

}
