package scustomer

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *SCustomerApi) handleCreateUser(c *gin.Context) {
	var usr *models.User
	rest.AssertNil(c.BindJSON(&usr))
	rest.AssertNil(usr.Create())
	s.SendData(c, usr)
}

func (s *SCustomerApi) handleUpdateUser(c *gin.Context) {
	var usr *models.User
	rest.AssertNil(c.BindJSON(&usr))
	rest.AssertNil(usr.Update())
	s.SendData(c, usr)
}

func (s *SCustomerApi) handleGetUsers(c *gin.Context) {
	var role = c.Query("role")
	var users, err = models.GetUserByRole(role)
	rest.AssertNil(err)
	s.SendData(c, users)
}

func (s *SCustomerApi) handleDeleteUser(c *gin.Context) {
	var err = models.DeleteUserByID(c.Query("id"))
	rest.AssertNil(err)
	s.Success(c)
}
