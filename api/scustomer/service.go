package scustomer

import (
	"fmt"

	"gitlab.com/trunglen/ss-queue/models"

	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
)

func (s *SCustomerApi) handleCreateService(c *gin.Context) {
	var service *models.Service
	c.BindJSON(&service)
	rest.AssertNil(service.Create())
	// cache.ServiceCache[service.ID] = service
	s.SendData(c, service)
}

func (s *SCustomerApi) handleUpdateService(c *gin.Context) {
	var service *models.Service
	c.BindJSON(&service)
	rest.AssertNil(service.Update())
	// cache.ServiceCache[service.ID] = service
	s.SendData(c, service)
}

func (s *SCustomerApi) handleGetServices(c *gin.Context) {
	var services, err = models.GetAllServices()
	rest.AssertNil(err)
	s.SendData(c, services)
}

func (s *SCustomerApi) handleDeleteServices(c *gin.Context) {
	var serviceID = c.Query("id")
	var counters, _ = models.GetCountersByServiceID(serviceID)
	if counters != nil && len(counters) > 0 {
		var counter = counters[0]
		rest.AssertNil(rest.BadRequest(fmt.Sprintf("Không thể xóa dịch vụ vì quầy %d đang sử dụng. Vui lòng thay đổi cấu hình quầy!", counter.CNum)))
	}

	var kiosks, _ = models.GetKiosksByServiceID(serviceID)
	if kiosks != nil && len(kiosks) > 0 {
		var kiosk = kiosks[0]
		rest.AssertNil(rest.BadRequest(fmt.Sprintf("Không thể xóa dịch vụ vì %s đang sử dụng. Vui lòng thay đổi cấu hình kiosk!", kiosk.Name)))
	}
	var err = models.DeleteServiceByID(serviceID)
	rest.AssertNil(err)
	s.Success(c)
}
