package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateTLayout(c *gin.Context) {
	var tlayout *models.TLayout
	rest.AssertNil(c.BindJSON(&tlayout))
	rest.AssertNil(tlayout.Create())
	s.SendData(c, tlayout)
}

func (s *AdminApi) handleUpdateTLayouts(c *gin.Context) {
	var tlayout *models.TLayout
	rest.AssertNil(c.BindJSON(&tlayout))
	rest.AssertNil(tlayout.Update())
	var kiosks, _ = models.GetKioskByTLayoutID(tlayout.ID)
	if kiosks != nil {
		// for _, kiosk := range kiosks {
		// 	evb.Publish(kiosk.ID, evb.KIOSK_RELOAD_EVENT)
		// }
	}
	s.SendData(c, tlayout)
}

func (s *AdminApi) handleDeleteTLayouts(c *gin.Context) {
	rest.AssertNil(models.DeleteTLayoutByID(c.Query("id")))
	s.Success(c)
}

func (s *AdminApi) handleGetTLayouts(c *gin.Context) {
	var tlayouts, err = models.GetAllTLayouts()
	rest.AssertNil(err)
	s.SendData(c, tlayouts)
}
