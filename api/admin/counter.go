package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateCounter(c *gin.Context) {
	var counter *models.Counter
	c.BindJSON(&counter)
	rest.AssertNil(counter.Create())
	// cache.CounterCache[counter.ID] = counter
	s.SendData(c, counter)
}

func (s *AdminApi) handleUpdateCounter(c *gin.Context) {
	var counter *models.Counter
	c.BindJSON(&counter)
	rest.AssertNil(counter.Update())
	// cache.CounterCache[counter.ID] = counter
	// go func() {
	// 	evb.Publish(counter.ID, evb.COUNTER_RELOAD_EVENT)
	// }()
	s.SendData(c, counter)
}

func (s *AdminApi) handleListCounter(c *gin.Context) {
	var counters, err = models.GetAllCounters()
	rest.AssertNil(err)
	s.SendData(c, counters)
}

func (s *AdminApi) handleDeleteCounter(c *gin.Context) {
	var err = models.DeleteCounterByID(c.Query("id"))
	rest.AssertNil(err)
	s.Success(c)
}
