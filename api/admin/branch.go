package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateBranch(c *gin.Context) {
	var branch *models.Branch
	c.BindJSON(&branch)
	rest.AssertNil(branch.Create())
	s.SendData(c, branch)
}

func (s *AdminApi) handleUpdateBranch(c *gin.Context) {
	var branch *models.Branch
	c.BindJSON(&branch)
	rest.AssertNil(branch.Update())
	s.SendData(c, branch)
}

func (s *AdminApi) handleDeleteBranch(c *gin.Context) {
	var id = c.Query("id")
	var err = models.DeleteBranchByID(id)
	rest.AssertNil(err)
	s.Success(c)
}

func (s *AdminApi) handleGetBranches(c *gin.Context) {
	var branches, err = models.GetAllBranches()
	rest.AssertNil(err)
	s.SendData(c, branches)
}
