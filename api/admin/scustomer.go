package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) createCustomer(c *gin.Context) {
	var customer *models.SCustomer
	rest.AssertNil(c.BindJSON(&customer))
	rest.AssertNil(customer.Create())
	s.SendData(c, customer)
}

func (s *AdminApi) updateCustomer(c *gin.Context) {
	var customer *models.SCustomer
	rest.AssertNil(c.BindJSON(&customer))
	rest.AssertNil(customer.Update())
	s.SendData(c, customer)
}

func (s *AdminApi) getCustomers(c *gin.Context) {
	var customers, err = models.FindAllSCustomer()
	rest.AssertNil(err)
	s.SendData(c, customers)
}

func (s *AdminApi) getCustomerDetail(c *gin.Context) {
	var customer, err = models.FindSCustomerByID(c.Query("id"))
	rest.AssertNil(err)
	s.SendData(c, customer)
}
