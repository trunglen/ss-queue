package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateKiosk(c *gin.Context) {
	var kiosk *models.Kiosk
	rest.AssertNil(c.BindJSON(&kiosk))
	rest.AssertNil(kiosk.Create())
	s.SendData(c, kiosk)
}

func (s *AdminApi) handleGetKiosks(c *gin.Context) {
	var kiosks, err = models.GetAllKiosks()
	rest.AssertNil(err)
	s.SendData(c, kiosks)
}

func (s *AdminApi) handleUpdateKiosk(c *gin.Context) {
	var kiosk *models.Kiosk
	rest.AssertNil(c.BindJSON(&kiosk))
	rest.AssertNil(kiosk.Update())
	// go func() {
	// 	evb.Publish(kiosk.ID, evb.KIOSK_RELOAD_EVENT)
	// }()
	s.SendData(c, kiosk)
}

func (s *AdminApi) handleDeleteKiosk(c *gin.Context) {
	var err = models.DeleteKioskByID(c.Query("id"))
	rest.AssertNil(err)
	s.Success(c)
}
