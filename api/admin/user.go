package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateUser(c *gin.Context) {
	var usr *models.User
	rest.AssertNil(c.BindJSON(&usr))
	rest.AssertNil(usr.Create())
	s.SendData(c, usr)
}

func (s *AdminApi) handleUpdateUser(c *gin.Context) {
	var usr *models.User
	rest.AssertNil(c.BindJSON(&usr))
	rest.AssertNil(usr.Update())
	s.SendData(c, usr)
}

func (s *AdminApi) handleGetUsers(c *gin.Context) {
	var role = c.Query("role")
	var users, err = models.GetUserByRole(role)
	rest.AssertNil(err)
	s.SendData(c, users)
}

func (s *AdminApi) handleDeleteUser(c *gin.Context) {
	var err = models.DeleteUserByID(c.Query("id"))
	rest.AssertNil(err)
	s.Success(c)
}
