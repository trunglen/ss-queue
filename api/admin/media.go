package admin

import (
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/gosimple/slug"
	"github.com/trunglen/g/x/math"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/helpers"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleCreateMedia(c *gin.Context) {
	file, err := c.FormFile("file")
	rest.AssertNil(err)
	var extension = filepath.Ext(file.Filename)
	var name = file.Filename[0 : len(file.Filename)-len(extension)]
	var path = "upload/" + slug.Make(name) + extension
	if _, err := os.Stat(path); err == nil {
		path = "upload/" + slug.Make(name) + "-" + math.RandStringNoPrefix(3) + extension
	}
	err = c.SaveUploadedFile(file, path)
	rest.AssertNil(err)
	var media = &models.Media{
		Name: slug.Make(name),
		Path: path,
		Ext:  extension,
		Size: helpers.CovertFileSize(file.Size),
	}
	switch extension {
	case ".jpg", ".png", ".gif", ".webp":
		media.Type = "image"
	case ".mp4", ".mov", ".avi":
		media.Type = "video"
	default:
		media.Type = "unknown"
	}
	rest.AssertNil(media.Create())
	s.SendData(c, map[string]interface{}{
		"url": media.Path,
	})
}

func (s *AdminApi) handleGetMedias(c *gin.Context) {
	var medias, err = models.GetAllMedias()
	rest.AssertNil(err)
	s.SendData(c, medias)
}

func (s *AdminApi) handleDeleteMedia(c *gin.Context) {
	var media, err = models.GetMediaByID(c.Query("id"))
	rest.AssertNil(err)
	os.Remove(media.Path)
	rest.AssertNil(models.DeleteMediaByID(c.Query("id")))
	s.Success(c)
}
