package admin

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *AdminApi) handleHistory(c *gin.Context) {
	// var branchs = web.GetArrString("branch_id", ",", c)
	// var start = web.MustGetInt64("start", c)
	// var end = web.MustGetInt64("end", c)
	// var waitStart, _ = strconv.Atoi(c.Query("wait_start"))
	// var waitEnd, _ = strconv.Atoi(c.Query("wait_end"))
	// var sers = web.GetArrString("service", ",", c)
	// var objSearch = ticket.ObjectSearch{
	// 	BranchIDs:  branchs,
	// 	TimeEnd:    end,
	// 	TimeStart:  start,
	// 	WaitStart:  waitStart,
	// 	WaitEnd:    waitEnd,
	// 	ServiceIDs: sers,
	// }
	var tickets, err = models.GetTickets()
	rest.AssertNil(err)
	s.SendData(c, tickets)
}

func (s *AdminApi) handleQuantityChart(c *gin.Context) {
	var report, err = models.GetGeneralReportLast7Days()
	rest.AssertNil(err)
	s.SendData(c, report)
}

func (s *AdminApi) handleGeneralReport(c *gin.Context) {
	var report, err = models.GetGeneralReport()
	rest.AssertNil(err)
	s.SendData(c, report)
}
