package customer

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/models"
)

func (s *CustomerApi) createCounter(c *gin.Context) {
	var counter *models.Counter
	c.BindJSON(&counter)
	rest.AssertNil(counter.Create())
	s.SendData(c, counter)
}

func (s *CustomerApi) updateCounter(c *gin.Context) {
	var counter *models.Counter
	c.BindJSON(&counter)
	rest.AssertNil(counter.Update())
	s.SendData(c, counter)
}

func (s *CustomerApi) listCounter(c *gin.Context) {
	var counters, err = models.GetAllCounters()
	rest.AssertNil(err)
	s.SendData(c, counters)
}

func (s *CustomerApi) deleteCounter(c *gin.Context) {
	rest.AssertNil(models.DeleteCounterByID(c.Query("id")))
	s.Success(c)
}
