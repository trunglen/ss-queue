package customer

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
)

type CustomerApi struct {
	rest.JsonRender
}

func NewCustomerApi(router *gin.RouterGroup) {
	var s = new(CustomerApi)
	router.POST("counter/create", s.createCounter)
	router.POST("counter/update", s.updateCounter)
	router.GET("counter/list", s.listCounter)
	router.DELETE("counter/delete", s.deleteCounter)
}
