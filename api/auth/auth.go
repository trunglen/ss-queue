package auth

import (
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/middlewares"
)

type AuthApi struct {
	rest.JsonRender
}

func NewAuthApi(router *gin.RouterGroup) {
	router.POST("admin/login", middlewares.AuthenticateAdmin().LoginHandler)
	// s.PUT("", s.Update)
}
