package common

import "github.com/trunglen/g/x/rest"

type ErrorCode string

const (
	NotFoundUserErrCode       = rest.BadRequest("USER_NOT_FOUND")
	NotFoundCustomerErrCode   = rest.BadRequest("CUSTOMER_NOT_FOUND")
	ExistCustomerEmailErrCode = rest.BadRequest("EXIST_CUSTOMER_EMAIL")
	ExistCustomerPhoneErrCode = rest.BadRequest("EXIST_CUSTOMER_PHONE")

	WrongLoginErrCode    = rest.BadRequest("WRONG_LOGIN")
	ExistUserErrCode     = rest.BadRequest("EXIST_USER")
	ExistCustomerErrCode = rest.BadRequest("EXIST_CUSTOMER")
	NoErrCode            = rest.BadRequest("0")
)

type SuccessCode string

const (
	CommonSuccessCode = SuccessCode("0")
)
