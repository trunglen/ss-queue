module gitlab.com/trunglen/ss-queue

go 1.15

require (
	firebase.google.com/go v3.13.0+incompatible
	firebase.google.com/go/v4 v4.5.0 // indirect
	github.com/appleboy/gin-jwt v2.5.0+incompatible
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/structs v1.1.0
	github.com/gin-gonic/contrib v0.0.0-20201005132743-ca038bbf2944
	github.com/gin-gonic/gin v1.6.3
	github.com/gosimple/slug v1.9.0
	github.com/jinzhu/inflection v1.0.0
	github.com/jinzhu/now v1.1.1
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/trunglen/g v0.0.0-20200304102400-24b1178fcc95
	go.mongodb.org/mongo-driver v1.4.2
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/api v0.40.0
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
