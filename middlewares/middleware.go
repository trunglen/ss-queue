package middlewares

import (
	"context"
	"errors"
	"net/http"
	"strings"
	"time"

	"firebase.google.com/go/auth"
	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
	"github.com/trunglen/g/x/math"
	"github.com/trunglen/g/x/rest"
	"gitlab.com/trunglen/ss-queue/helpers/logger"
	"gitlab.com/trunglen/ss-queue/models"
)

//CORSMiddleware ...
//CORS (Cross-Origin Resource Sharing)
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:4200")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, x-access-token")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}

//RequestIDMiddleware ...
//Generate a unique ID and attach it to each request for future reference or use
func RequestIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		uuid := math.RandString("", 11)
		c.Writer.Header().Set("X-Request-Id", uuid)
		c.Next()
	}
}

//Recovery ...
//Handle Error
func Recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				logger.Log.Info(err)
				var errResponse = map[string]interface{}{
					"error":  err.(error).Error(),
					"status": "error",
				}
				if httpError, ok := err.(rest.IHttpError); ok {
					c.AbortWithStatusJSON(httpError.StatusCode(), errResponse)
				} else {
					c.AbortWithStatusJSON(500, errResponse)
				}
			}
		}()
		c.Next()
	}
}

const (
	realm       = "Smartsol"
	secretKey   = "Thanhtruc05062017"
	identityKey = "user_info"
)

var (
	userModel = new(models.User)
)

type InfoAttacher struct {
	UserID string
	Email  string
	Name   string
	Role   string
}

type LoginInfo struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func AuthenticateAdmin() *jwt.GinJWTMiddleware {
	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       realm,
		Key:         []byte(secretKey),
		Timeout:     24 * time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &InfoAttacher{
				Email:  claims["email"].(string),
				Name:   claims["name"].(string),
				Role:   claims["role"].(string),
				UserID: claims["user_id"].(string),
			}
		},
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*InfoAttacher); ok {
				return jwt.MapClaims{
					"email":   v.Email,
					"name":    v.Name,
					"role":    v.Role,
					"user_id": v.UserID,
				}
			}
			return jwt.MapClaims{}
		},
		LoginResponse: func(c *gin.Context, code int, token string, time time.Time) {
			c.JSON(http.StatusOK, gin.H{
				"status": "success",
				"data": map[string]interface{}{
					"access_token": token,
					"user":         userModel,
				},
			})
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginInfo LoginInfo
			var err error
			if err = c.ShouldBind(&loginInfo); err != nil {
				return "", jwt.ErrMissingLoginValues
			}
			userModel, err = userModel.Login(loginInfo.Email, loginInfo.Password)
			if userModel == nil {
				return "", errors.New("Sai tên đăng nhập hoặc mật khẩu")
			}
			return &InfoAttacher{
				Email:  userModel.Email,
				Name:   userModel.Name,
				Role:   userModel.Role,
				UserID: userModel.ID,
			}, nil
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			// var requestPath = c.Request.URL.String()
			// if v, ok := data.(*InfoAttacher); ok {
			// 	if ((v.Role == "admin" || v.Role == "super-admin") && strings.Contains(requestPath, adminResource)) ||
			// 		(v.Role == "operator" && strings.Contains(requestPath, operatorResource)) ||
			// 		strings.Contains(requestPath, commonResource) {
			// 		return true
			// 	}
			// 	return false
			// }
			return true
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"status": "error",
				"error":  message,
			})
		},
		TokenLookup:   "header: Authorization, query: token, cookie: jwt",
		TokenHeadName: "Bearer",
		TimeFunc:      time.Now,
	})
	if err != nil {
		return nil
	}
	return authMiddleware
}

// AuthMiddleware : to verify all authorized operations
func FirebaseAuthMiddleware(c *gin.Context) {
	firebaseAuth := c.MustGet("firebaseAuth").(*auth.Client)

	authorizationToken := c.GetHeader("Authorization")
	idToken := strings.TrimSpace(strings.Replace(authorizationToken, "Bearer", "", 1))

	if idToken == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Id token not available"})
		c.Abort()
		return
	}

	//verify token
	token, err := firebaseAuth.VerifyIDToken(context.Background(), idToken)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid token"})
		c.Abort()
		return
	}

	c.Set("UUID", token.UID)
	c.Next()
}
